# Makefile to interact with terraform

ifndef ENVNAME
$(error ENVNAME is not set)
endif

ifndef REGION
$(error REGION is not set)
endif

# vars to import
VARS=-var "envtype=$(ENVNAME)" \
-var "envname=$(ENVNAME)" \
-var "aws_region=$(REGION)"


# Build tfvars hierarchy for import
VARFILE=
ifneq (,$(wildcard params/default.tfvars))
    VARFILE=-var-file=params/default.tfvars
endif
ifneq (,$(wildcard params/$(ENVNAME).tfvars))
    VARFILE+= -var-file=params/$(ENVNAME).tfvars
endif
ifneq (,$(wildcard params/$(ENVNAME)_$(REGION).tfvars))
    VARFILE+= -var-file=params/$(ENVNAME)_$(REGION).tfvars
endif


export AWS_DEFAULT_REGION=$(REGION)

begin: plan

clean:
	rm -rf .terraform ./modules tfplan

init: clean
	terraform get .
#	[ -d backend-config/$(ENVNAME) ] || mkdir -p backend-config/$(ENVNAME)
#	cp backend-config/template.json backend-config/$(ENVNAME)/backend.json
	#sed -i "s/ENVNAME/$(ENVNAME)/g" backend-config/$(ENVNAME)/backend.json
	#sed -i "s/REGION/$(REGION)/g" backend-config/$(ENVNAME)/backend.json
	terraform init -backend-config=backend-config/$(ENVNAME)/backend.json
#	rm -fr backend-config/$(ENVNAME)

refresh: init
	terraform refresh $(VARS) $(VARFILE) .

plan: init
	terraform plan $(VARS) $(VARFILE) -out tfplan .

apply:
	terraform apply tfplan
	rm tfplan

destroy: init
	terraform plan -destroy $(VARS) $(VARFILE) .
	terraform destroy $(VARS) $(VARFILE) .
