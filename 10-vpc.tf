## VPC
resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr}"

  tags {
    Name        = "${var.envname}-vpc"
    Environment = "${var.envname}"
  }
}

resource "aws_vpc_dhcp_options" "vpc" {
  domain_name         = "${var.domain}"
  domain_name_servers = ["${var.domain_name_servers}"]

  tags {
    Name        = "${var.envname}-vpc"
    Environment = "${var.envname}"
  }
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp" {
  vpc_id          = "${aws_vpc.vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.vpc.id}"
}

## Public Subnets
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.envname}-vpc"
    Environment = "${var.envname}"
  }
}

resource "aws_subnet" "public" {
  count                   = "${length(var.public_subnets)}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${element(var.public_subnets, count.index)}"
  availability_zone       = "${element(split(",",lookup(var.aws_zones,var.aws_region)), count.index)}"
  map_public_ip_on_launch = "false"

  tags {
    Name        = "${var.envname}-public"
    Environment = "${var.envname}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags {
    Name        = "${var.envname}-public"
    Environment = "${var.envname}"
  }
}

resource "aws_route_table_association" "public" {
  count          = "${length(var.public_subnets)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

## Private Subnets
resource "aws_subnet" "private" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${element(var.private_subnets, count.index)}"
  availability_zone = "${element(split(",",lookup(var.aws_zones,var.aws_region)), count.index)}"
  count             = "${length(var.private_subnets)}"

  tags {
    Name        = "${var.envname}-private"
    Environment = "${var.envname}"
  }
}

resource "aws_route_table" "private" {
  count            = "${length(var.private_subnets)}"
  vpc_id           = "${aws_vpc.vpc.id}"
  propagating_vgws = ["${var.vgws_to_propagate}"]

  tags {
    Name        = "${var.envname}-private"
    Environment = "${var.envname}"
  }
}

resource "aws_route_table_association" "private" {
  count          = "${length(var.private_subnets)}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

resource "aws_eip" "nat" {
  depends_on = ["aws_internet_gateway.igw"]
  count      = "${length(var.natgw_eips) == 0 ? length(var.public_subnets) : 0}"
  vpc        = true
}

resource "aws_nat_gateway" "natgw" {
  count         = "${length(var.public_subnets)}"
  allocation_id = "${element(concat(var.natgw_eips, aws_eip.nat.*.id), count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
}

resource "aws_route" "private_default" {
  count                  = "${length(var.private_subnets)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.natgw.*.id, count.index)}"
  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
}
