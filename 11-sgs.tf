#Unfiltered Egress Security Group
resource "aws_security_group" "unfiltered-vpc-egress-sg" {
  name        = "${var.envname}-sg-egress-sg"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security group for unfiltered-vpc egress"

  tags {
    Name = "${var.envname}-sg-unfiltered-vpc-egress"
  }
}

resource "aws_security_group_rule" "unfiltered-vpc-egress-sg" {
  type              = "egress"
  security_group_id = "${aws_security_group.unfiltered-vpc-egress-sg.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}
