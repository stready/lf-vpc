
variable "envname"  {}

variable "aws_region" {
  default = "us-west-2"
}

variable "aws_zones" {
  type = "map"

  default = {
    us-west-2 = "us-west-2a,us-west-2b"
  }
}

variable vpc_cidr  {} 

variable "public_subnets" {
  type    = "list"
  default = []
}

variable "private_subnets" {
  type    = "list"
  default = []
}

variable "domain" {}

variable "domain_name_servers" {
  default = ["127.0.0.1", "AmazonProvidedDNS"]
}

variable "vgws_to_propagate" {
  type    = "list"
  default = []
}

variable "natgw_eips" {
  type    = "list"
  default = []
}
