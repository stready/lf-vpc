output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "vpc_cidr" {
  value = "${var.vpc_cidr}"
}

output "vpc_public_subnets" {
  value = "${var.public_subnets}"
}

output "vpc_private_subnets" {
  value = "${var.private_subnets}"
}


output "unfiltered-vpc-egress-sg" {
  value = "${aws_security_group.unfiltered-vpc-egress-sg.id}"
}

output "vpc_availability_zones" {
  value = ["${var.aws_zones}"]
}
